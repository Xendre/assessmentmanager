﻿using Repartition.Graph;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using System;
using Repartition;

namespace TestGraph
{
    
    
    /// <summary>
    ///Classe de test pour GraphTest, destinée à contenir tous
    ///les tests unitaires GraphTest
    ///</summary>
    [TestClass]
    public class GraphTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Obtient ou définit le contexte de test qui fournit
        ///des informations sur la série de tests active ainsi que ses fonctionnalités.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Attributs de tests supplémentaires
        // 
        //Vous pouvez utiliser les attributs supplémentaires suivants lorsque vous écrivez vos tests :
        //
        //Utilisez ClassInitialize pour exécuter du code avant d'exécuter le premier test dans la classe
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Utilisez ClassCleanup pour exécuter du code après que tous les tests ont été exécutés dans une classe
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Utilisez TestInitialize pour exécuter du code avant d'exécuter chaque test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Utilisez TestCleanup pour exécuter du code après que chaque test a été exécuté
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///Test pour Constructeur Graph`1
        ///</summary>
        public void GraphConstructorTestHelper<T>()
        {
            NodeList<T> nodeSet = new NodeList<T>();
            nodeSet.Add(new Node<T>());
            Graph<T> target = new Graph<T>(nodeSet);
            Assert.AreEqual(1, target.Count);
        }

        [TestMethod]
        public void GraphConstructorTest()
        {
            GraphConstructorTestHelper<int>();
        }

        /// <summary>
        ///Test pour Constructeur Graph`1
        ///</summary>
        public void GraphConstructorTest1Helper<T>()
        {
            Graph<T> target = new Graph<T>();
            Assert.IsNotNull(target);
        }

        [TestMethod()]
        public void GraphConstructorTest1()
        {
            GraphConstructorTest1Helper<int>();
        }

        /// <summary>
        ///Test pour AddDirectedEdge
        ///</summary>
        public void AddDirectedEdgeTestHelper<T>()
        {
            Graph<T> target = new Graph<T>();
            GraphNode<T> from = new GraphNode<T>();
            GraphNode<T> to = new GraphNode<T>();
            target.AddNode(from);
            target.AddNode(to);
            int cost = 1;
            target.AddDirectedEdge(from, to, cost);
            Assert.AreEqual(1, from.Neighbors.Count);
        }

        [TestMethod()]
        public void AddDirectedEdgeTest()
        {
            AddDirectedEdgeTestHelper<int>();
        }

        /// <summary>
        ///Test pour AddNode
        ///</summary>
        public void AddNodeTestHelper<T>()
        {
            Graph<T> target = new Graph<T>();
            T value = default(T);
            target.AddNode(value);
            Assert.AreEqual(1, target.Count);
        }

        [TestMethod()]
        public void AddNodeTest()
        {
            AddNodeTestHelper<int>();
        }

        /// <summary>
        ///Test pour AddNode
        ///</summary>
        public void AddNodeTest1Helper<T>()
        {
            Graph<T> target = new Graph<T>();
            GraphNode<T> node = new GraphNode<T>();
            target.AddNode(node);
            Assert.AreEqual(1, target.Count);
        }

        [TestMethod()]
        public void AddNodeTest1()
        {
            AddNodeTest1Helper<int>();
        }

        /// <summary>
        ///Test pour AddUndirectedEdge
        ///</summary>
        public void AddUndirectedEdgeTestHelper<T>()
        {
            Graph<T> target = new Graph<T>();
            GraphNode<T> from = new GraphNode<T>();
            GraphNode<T> to = new GraphNode<T>();
            target.AddNode(from);
            target.AddNode(to);
            int cost = 1;
            target.AddUndirectedEdge(from, to, cost);
            Assert.AreEqual(2, from.Neighbors.Count + to.Neighbors.Count);
        }

        [TestMethod()]
        public void AddUndirectedEdgeTest()
        {
            AddUndirectedEdgeTestHelper<int>();
        }

        /// <summary>
        ///Test pour Contains
        ///</summary>
        public void ContainsTestHelper<T>()
        {
            Graph<T> target = new Graph<T>();
            T value = default(T);
            target.AddNode(value);
            
            bool expected = true;
            bool actual;
            actual = target.Contains(value);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void ContainsTest()
        {
            ContainsTestHelper<int>();
        }

        /// <summary>
        ///Test pour Remove
        ///</summary>
        public void RemoveTestHelper<T>()
        {
            Graph<T> target = new Graph<T>();
            T value = default(T);
            target.AddNode(value);
            bool expected = true;
            bool actual;
            actual = target.Remove(value);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void RemoveTest()
        {
            RemoveTestHelper<int>();
        }

        /// <summary>
        ///Test pour Count
        ///</summary>
        public void CountTestHelper<T>()
        {
            Graph<T> target = new Graph<T>();
            T value = default(T);
            target.AddNode(value);
            int actual;
            actual = target.Count;
            Assert.AreEqual(1, actual);
        }

        [TestMethod()]
        public void CountTest()
        {
            CountTestHelper<int>();
        }

        /// <summary>
        ///Test pour Nodes
        ///</summary>
        public void NodesTestHelper<T>()
        {
            Graph<T> target = new Graph<T>();
            T value = default(T);
            target.AddNode(value);
            NodeList<T> actual;
            actual = target.Nodes;
            Assert.AreEqual(1, actual.Count);
        }

        [TestMethod()]
        public void NodesTest()
        {
            NodesTestHelper<int>();
        }

        public void ColorGraphConstructorTestHelper()
        {
            GraphNode<int> n1 = new GraphNode<int>(1);
            GraphNode<int> n2 = new GraphNode<int>(2);
            GraphNode<int> n3 = new GraphNode<int>(3);
            GraphNode<int> n4 = new GraphNode<int>(4);
            GraphNode<int> n5 = new GraphNode<int>(5);
            GraphNode<int> n6 = new GraphNode<int>(6);

            Graph<int> target = new Graph<int>();
            target.AddNode(n1);
            target.AddNode(n2);
            target.AddNode(n3);
            target.AddNode(n4);
            target.AddNode(n5);
            target.AddNode(n6);

            target.AddUndirectedEdge(n1, n2, 1);
            target.AddUndirectedEdge(n1, n3, 1);
            target.AddUndirectedEdge(n2, n4, 1);
            target.AddUndirectedEdge(n1, n5, 1);
            target.AddUndirectedEdge(n3, n6, 1);

            target.Colorize();

            for (int i = 0; i < target.Nodes.Count;i++ )
            {
                System.Diagnostics.Debug.WriteLine("NODE VALUE:" + target.SortedNodes[i].Value);
            }

            Assert.AreEqual(6, target.Nodes.Count);
        }
        [TestMethod()]
        public void ColorGraphConstructorTest()
        {
            ColorGraphConstructorTestHelper();
        }
    }
}
