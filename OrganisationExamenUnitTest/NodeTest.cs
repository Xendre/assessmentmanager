﻿using Repartition;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using System;

namespace TestGraph
{
    
    
    /// <summary>
    ///Classe de test pour NodeTest, destinée à contenir tous
    ///les tests unitaires NodeTest
    ///</summary>
    [TestClass()]
    public class NodeTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Obtient ou définit le contexte de test qui fournit
        ///des informations sur la série de tests active ainsi que ses fonctionnalités.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Attributs de tests supplémentaires
        // 
        //Vous pouvez utiliser les attributs supplémentaires suivants lorsque vous écrivez vos tests :
        //
        //Utilisez ClassInitialize pour exécuter du code avant d'exécuter le premier test dans la classe
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Utilisez ClassCleanup pour exécuter du code après que tous les tests ont été exécutés dans une classe
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Utilisez TestInitialize pour exécuter du code avant d'exécuter chaque test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Utilisez TestCleanup pour exécuter du code après que chaque test a été exécuté
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///Test pour Constructeur Node`1
        ///</summary>
        public void NodeConstructorTest1Helper<T>()
        {
            T data = default(T); // TODO: initialisez à une valeur appropriée
            Node<T> target = new Node<T>(data);
            Assert.AreEqual(data, target.Value);
        }

        [TestMethod()]
        public void NodeConstructorTest1()
        {
            NodeConstructorTest1Helper<int>();
        }

        /// <summary>
        ///Test pour Constructeur Node`1
        ///</summary>
        public void NodeConstructorTest2Helper<T>()
        {
            Node<T> target = new Node<T>();
            Assert.IsNotNull(target);
        }

        [TestMethod()]
        public void NodeConstructorTest2()
        {
            NodeConstructorTest2Helper<int>();
        }

        /// <summary>
        ///Test pour Value
        ///</summary>
        public void ValueTestHelper<T>()
        {
            Node<T> target = new Node<T>();
            T expected = default(T);
            T actual;
            target.Value = expected;
            actual = target.Value;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void ValueTest()
        {
            ValueTestHelper<int>();
        }

    }
}
