﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using Windows.Storage;
using System.Threading.Tasks;
using OrganisationExamenModel.Objects;
using OrganisationExamenModel.Services.Binder.Interfaces;
using OrganisationExamenModel.Services.Binder;
using System.IO;
using OrganisationExamenModel.Services.Converter;
using OrganisationExamenModel.Services.Serializer;
using OrganisationExamenModel.Services.Scheduler;

namespace OrganisationExamenUnitTest
{
    [TestClass]
    public class CSVXMLConverterUnitTest
    {
        [TestMethod]
        public async Task testTestBinder()
        {
            String data = "Nom;Duree;Type\nLatin;1h;e\nMath;2h;e\nFrançais;1h;e\nHistoire;1h;e\nSport;2h;e\nAnglais;2h;e\nMath Spe;4h;e\nHistoire;15min;o\nLatin;10h;a\nMath;2b;z\nFrançais;m10;e\nHistoire;h1;e\nSport;2h;e\nAnglais;2h;e\nMath Spe;4h;r\nHistoire;15min;g";
            String[] lines = data.Split('\n');
            IBinder<Test> binder = new TestBinder();
            List<object> tests = new List<object>();
            for (int i = 1; i < lines.Length; i++)
            {
                Test t = binder.getBindedObject(lines[i]);
                tests.Add(binder.getBindedObject(lines[i]));
            }
            StorageFile output = await ApplicationData.Current.LocalFolder.CreateFileAsync("Tests.xml",CreationCollisionOption.ReplaceExisting);
            await SerializationHelper.SaveAsync<Test>(ApplicationData.Current.LocalFolder, output, tests);
            Assert.IsTrue(true);
        }

        [TestMethod]
        public async Task testConverter()
        {
            var ipath = "Testsalle.csv";
            var folder = Windows.ApplicationModel.Package.Current.InstalledLocation;
            StorageFolder ofolder = ApplicationData.Current.LocalFolder;
            StorageFile input = await folder.GetFileAsync(ipath);
            Assert.AreEqual( (await CSVConverter.convertFile(input, ofolder)).Count,0);
        }
        [TestMethod]
        public async Task testConverterSupervisor()
        {
            var ipath = "testsurveillant.csv";
            var folder = Windows.ApplicationModel.Package.Current.InstalledLocation;
            var ofolder = ApplicationData.Current.LocalFolder;
            StorageFile input = await folder.GetFileAsync(ipath);
            Assert.AreEqual((await CSVConverter.convertFile(input, ofolder)).Count, 0);
        }
        [TestMethod]
        public async Task testCompletImport()
        {
            var folder = Windows.ApplicationModel.Package.Current.InstalledLocation;
            IEnumerable<StorageFile> files = null;
            List<object> sts ;
            if(folder!=null)
                files = await folder.GetFilesAsync();
            if (files != null && files.Count() >0)
            {
                var csvfiles = (from t1 in files where t1.FileType.ToLower() == ".csv" select t1);
                foreach (var csvfile in csvfiles)
                {
                    var ofolder = ApplicationData.Current.LocalFolder;
                    var a = (await CSVConverter.convertFile(csvfile, ofolder));
                }
                StorageFile xmlresult =await ApplicationData.Current.LocalFolder.GetFileAsync("student.xml");
                sts = await SerializationHelper.RestoreAsync<Student>(ApplicationData.Current.LocalFolder, xmlresult);
                IEnumerable<Student> students = sts.Cast<Student>();
                xmlresult = await ApplicationData.Current.LocalFolder.GetFileAsync("test.xml");
                sts = await SerializationHelper.RestoreAsync<Test>(ApplicationData.Current.LocalFolder, xmlresult);
                IEnumerable<Test> tests = sts.Cast<Test>();
                xmlresult = await ApplicationData.Current.LocalFolder.GetFileAsync("classes.xml");
                sts = await SerializationHelper.RestoreAsync<ClassRoom>(ApplicationData.Current.LocalFolder, xmlresult);
                IEnumerable<ClassRoom> classerooms = sts.Cast<ClassRoom>();
                xmlresult = await ApplicationData.Current.LocalFolder.GetFileAsync("supervisor.xml");
                sts = await SerializationHelper.RestoreAsync<Supervisor>(ApplicationData.Current.LocalFolder, xmlresult);
                IEnumerable<Supervisor> supervisors = sts.Cast<Supervisor>();
                Scheduler scheduler = new Scheduler();

                List<Classe> classes = new List<Classe>();
                Boolean flag = false;
                foreach (Student st in students)
                {
                    foreach (Classe cl in classes)
                    {
                        if (cl.ClasseName == st.Section)
                        {
                            cl.addStudent(st);
                            flag = true;
                            break;
                        }
                        else flag = false;
                    }
                    if (!flag)
                    {
                        Classe c = new Classe(st.Section);
                        c.addStudent(st);
                        classes.Add(c);
                        flag = true;
                    }
                }

                //Creation des Classes étudiants
                for (int i = 0; i < tests.Count<Test>(); i++)
                {
                    tests.ElementAt(i).Students = classes.ElementAt(i).Students;
                }
            }
            else
            {
                Assert.IsNull(files,"Files Nul");
            }
        }
    }
}
