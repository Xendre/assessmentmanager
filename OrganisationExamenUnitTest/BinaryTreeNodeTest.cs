﻿using Repartition;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using System;

namespace TestGraph
{
    
    
    /// <summary>
    ///Classe de test pour BinaryTreeNodeTest, destinée à contenir tous
    ///les tests unitaires BinaryTreeNodeTest
    ///</summary>
    [TestClass]
    public class BinaryTreeNodeTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Obtient ou définit le contexte de test qui fournit
        ///des informations sur la série de tests active ainsi que ses fonctionnalités.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Attributs de tests supplémentaires
        // 
        //Vous pouvez utiliser les attributs supplémentaires suivants lorsque vous écrivez vos tests :
        //
        //Utilisez ClassInitialize pour exécuter du code avant d'exécuter le premier test dans la classe
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Utilisez ClassCleanup pour exécuter du code après que tous les tests ont été exécutés dans une classe
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Utilisez TestInitialize pour exécuter du code avant d'exécuter chaque test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Utilisez TestCleanup pour exécuter du code après que chaque test a été exécuté
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///Test pour Constructeur BinaryTreeNode`1
        ///</summary>
        public void BinaryTreeNodeConstructorTestHelper<T>()
        {
            T data = default(T);
            BinaryTreeNode<T> left = new BinaryTreeNode<T>(default(T));
            BinaryTreeNode<T> right = new BinaryTreeNode<T>(default(T));
            BinaryTreeNode<T> target = new BinaryTreeNode<T>(data, left, right);
            Assert.IsNotNull(target.Value);
            Assert.IsNotNull(target.Left);
            Assert.IsNotNull(target.Right);
        }

        [TestMethod]
        public void BinaryTreeNodeConstructorTest()
        {
            BinaryTreeNodeConstructorTestHelper<int>();
        }

        /// <summary>
        ///Test pour Constructeur BinaryTreeNode`1
        ///</summary>
        public void BinaryTreeNodeConstructorTest1Helper<T>()
        {
            T data = default(T);
            BinaryTreeNode<T> target = new BinaryTreeNode<T>(data);
            Assert.IsNotNull(target.Value);
        }

        [TestMethod]
        public void BinaryTreeNodeConstructorTest1()
        {
            BinaryTreeNodeConstructorTest1Helper<int>();
        }

        /// <summary>
        ///Test pour Constructeur BinaryTreeNode`1
        ///</summary>
        public void BinaryTreeNodeConstructorTest2Helper<T>()
        {
            BinaryTreeNode<T> target = new BinaryTreeNode<T>();
            Assert.IsNotNull(target);
        }

        [TestMethod]
        public void BinaryTreeNodeConstructorTest2()
        {
            BinaryTreeNodeConstructorTest2Helper<int>();
        }

        /// <summary>
        ///Test pour Left
        ///</summary>
        public void LeftTestHelper<T>()
        {
            BinaryTreeNode<T> target = new BinaryTreeNode<T>();
            BinaryTreeNode<T> expected = new BinaryTreeNode<T>(default(T)) ;
            BinaryTreeNode<T> actual;
            target.Left = expected;
            actual = target.Left;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void LeftTest()
        {
            LeftTestHelper<int>();
        }

        /// <summary>
        ///Test pour Right
        ///</summary>
        public void RightTestHelper<T>()
        {
            BinaryTreeNode<T> target = new BinaryTreeNode<T>();
            BinaryTreeNode<T> expected = new BinaryTreeNode<T>(default(T));
            BinaryTreeNode<T> actual;
            target.Right = expected;
            actual = target.Right;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void RightTest()
        {
            RightTestHelper<int>();
        }
    }
}
