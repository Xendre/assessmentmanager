﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrganisationExamenModel.Services.Converter
{
    public enum ConverterType { STUDENTCSVTOXML, SUPERVISORCSVTOML, CLASSROMMCSVTOXML, TESTCSVTOXML };
    public interface IConvertertFactory
    {
        IFileConverter getConverter(ConverterType type);
    }
}
