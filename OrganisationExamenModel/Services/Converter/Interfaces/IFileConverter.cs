﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace OrganisationExamenModel.Services.Converter
{
    public interface IFileConverter
    {
        Task<List<ConvertError>> convert(StorageFile input, StorageFile output, IValidator validator);
    }
}
