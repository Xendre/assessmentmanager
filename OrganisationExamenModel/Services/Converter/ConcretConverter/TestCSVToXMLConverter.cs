﻿using OrganisationExamenModel.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Data.Xml.Dom;
using Windows.Storage;

namespace OrganisationExamenModel.Services.Converter
{
    public class TestCSVToXMLConverter:IFileConverter
    {
        public async Task<List<ConvertError>> convert(StorageFile input, StorageFile output, IValidator validator)
        {
            List<ConvertError> errors = new List<ConvertError>();
            List<object> tests = new List<object>();
            string text=null;
            try
            {
                text = await Windows.Storage.FileIO.ReadTextAsync(input);
            }
            catch (Exception e)
            {
                errors.Add(new ConvertError(0,0, ConvertError.ErrorType.WRONG_FILE_FORMAT, e.Message));
                return errors;
            }
            if (!validator.validate(text))
            {
                errors.Add(new ConvertError(0, 0, ConvertError.ErrorType.ELEMENTS_ARE_MISSING, "Invalid File"));
            }
            else
            {
                String[] lines = text.Split('\n');
                text = null;
                String[] baliseName = lines[0].Split(';');

                for (int i = 1; i < lines.Length; i++)
                {
                    String[] values = lines[i].Split(';');
                    if(values.Length>=3)
                        tests.Add(new Test { Name = values[0], Duration = values[1], Type = values[2], Id = "0" });
                }
                await Serializer.SerializationHelper.SaveAsync<Test>(ApplicationData.Current.LocalFolder, output, tests);
            }
            return errors;
        }
    }
}
