﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrganisationExamenModel.Services.Converter
{
    public class StudentCSVValidator:IValidator
    {
        public bool validate(string content)
        {
            if (content.Length == 0)
                return false;
            String[] lines = content.Split('\n');
            for (int i = 1; i < lines.Length; i++)
            {
                String line = lines[i];
                if (line.Length != 0)
                {
                    String[] elems = line.Split(';');
                    if (elems.Length == 5 || elems.Length == 6)
                    {
                        DateTime t;
                        if (!DateTime.TryParse(elems[2],out t))
                            return false;
                    }
                    else return false;
                }
            }
            return true;
        }
    }
}
