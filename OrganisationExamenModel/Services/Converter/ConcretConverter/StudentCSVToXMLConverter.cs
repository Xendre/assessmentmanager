﻿using OrganisationExamenModel.Objects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.Data.Xml.Dom;
using Windows.Storage;

namespace OrganisationExamenModel.Services.Converter
{
    public class StudentCSVToXMLConverter:IFileConverter
    {
        private List<ConvertError> errors = new List<ConvertError>();
        public async Task<List<ConvertError>> convert(StorageFile input, StorageFile output, IValidator validator)
        {
            List<ConvertError> errors = new List<ConvertError>();
            List<object> students = new List<object>();
            string text = null;
            try
            {
                text = await Windows.Storage.FileIO.ReadTextAsync(input);
            }
            catch (Exception e)
            {
                errors.Add(new ConvertError(0, 0, ConvertError.ErrorType.WRONG_FILE_FORMAT, e.Message));
                return errors;
            }
            if (!validator.validate(text))
            {
                errors.Add(new ConvertError(0, 0, ConvertError.ErrorType.SYNTAX_ERROR, ConvertError.ErrorType.SYNTAX_ERROR.ToString()));
            }
            else
            {
                String[] lines = text.Split('\n');
                text = null;
                String[] baliseName = lines[0].Split(';');

                for (int i = 1; i < lines.Length; i++)
                {
                    String[] values = lines[i].Split(';');
                    try
                    {
                        if (values.Length == 6)
                            students.Add(new Student { LastName = values[0], FirstName = values[1], Birthday = values[2], Section = values[3], ClassRoomName = values[4], InternNumber = values[5] });
                        else
                        {
                            if (values.Length == 5)
                                students.Add(new Student { LastName = values[0], FirstName = values[1], Birthday = values[2], Section = values[3], ClassRoomName = values[4], InternNumber = "" });
                        }
                    }
                    catch (Exception e)
                    {
                        errors.Add(new ConvertError(i, values.Length + 1, ConvertError.ErrorType.ELEMENTS_ARE_MISSING, e.Message));
                    }
                }
                await Serializer.SerializationHelper.SaveAsync<Student>(ApplicationData.Current.LocalFolder, output, students);
            }
            return errors;
        }
    }
}
