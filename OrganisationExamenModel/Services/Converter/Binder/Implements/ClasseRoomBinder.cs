﻿using OrganisationExamenModel.Objects;
using OrganisationExamenModel.Services.Binder.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrganisationExamenModel.Services.Binder
{
    public class ClasseRoomBinder:IBinder<ClassRoom>
    {
        public ClassRoom getBindedObject(string input)
        {
            String[] elems = input.Split(';');
            if (elems.Length == 3)
            {
                return new ClassRoom() { Num = elems[0], Designation = elems[1], Capacity = elems[2]};
            }
            return new ClassRoom() { Num = "", Designation = "", Capacity = "" }; 
        }
    }
}
