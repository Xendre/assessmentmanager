﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrganisationExamenModel.Services.Binder.Interfaces
{
    public interface IBinder<T>
    {
        T getBindedObject(String input);
    }
}
