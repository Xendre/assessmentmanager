﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repartition.Graph
{
    public class Graph<T>
    {
        protected NodeList<T> nodeSet;
        Queue<GraphNode<T>> fifo = new Queue<GraphNode<T>>();
        public List<GraphNode<T>> SortedNodes = new List<GraphNode<T>>();
        public Graph() : this(null) { }
        public Graph(NodeList<T> nodeSet)
        {
            if (nodeSet == null)
                this.nodeSet = new NodeList<T>();
            else
                this.nodeSet = nodeSet;
        }

        public void AddNode(GraphNode<T> node)
        {
            // adds a node to the graph
            nodeSet.Add(node);
        }

        public void AddNode(T value)
        {
            // adds a node to the graph
            nodeSet.Add(new GraphNode<T>(value));
        }

        public void AddDirectedEdge(GraphNode<T> from, GraphNode<T> to, int cost)
        {
            from.Neighbors.Add(to);
            from.Costs.Add(cost);
        }

        public void AddUndirectedEdge(GraphNode<T> from, GraphNode<T> to, int cost)
        {
            from.Neighbors.Add(to);
            from.Costs.Add(cost);

            to.Neighbors.Add(from);
            to.Costs.Add(cost);
        }

        public bool Contains(T value)
        {
            return nodeSet.FindByValue(value) != null;
        }

        public bool Remove(T value)
        {
            // first remove the node from the nodeset
            GraphNode<T> nodeToRemove = (GraphNode<T>)nodeSet.FindByValue(value);
            if (nodeToRemove == null)
                // node wasn't found
                return false;

            // otherwise, the node was found
            nodeSet.Remove(nodeToRemove);

            // enumerate through each node in the nodeSet, removing edges to this node
            foreach (GraphNode<T> gnode in nodeSet)
            {
                int index = gnode.Neighbors.IndexOf(nodeToRemove);
                if (index != -1)
                {
                    // remove the reference to the node and associated cost
                    gnode.Neighbors.RemoveAt(index);
                    gnode.Costs.RemoveAt(index);
                }
            }

            return true;
        }



        public NodeList<T> Nodes
        {
            get
            {
                return nodeSet;
            }
        }

        public int Count
        {
            get { return nodeSet.Count; }
        }

        public void Colorize()
        {
            GraphNode<T> currentNode = (GraphNode <T>) this.nodeSet.First();
            currentNode.Visited = true;
            SortedNodes.Add(currentNode);
            fifo.Enqueue(currentNode);
            while (fifo.Count != 0)
            {
                currentNode = fifo.Dequeue();
                int i = 0;
                while (i != currentNode.Neighbors.Count)
                {
                    GraphNode<T> son = ((GraphNode<T>)currentNode.Neighbors.ElementAt(i));
                    if (!son.Visited)
                    {
                        son.Visited = true;
                        SortedNodes.Add(son);
                        fifo.Enqueue(son);
                    }
                    i++;
                }
            }
            SortedNodes.Sort(delegate(GraphNode<T> n1, GraphNode<T> n2) { return -1*n1.Neighbors.Count.CompareTo(n2.Neighbors.Count); });
            int Color = 0;
            SortedNodes[0].Color = Color;
            
            foreach (GraphNode<T> Current in SortedNodes)
            {
                if (Current.Color != -1)
                {
                    foreach (GraphNode<T> node in SortedNodes)
                    {
                        if (node != Current && node.Color == -1)
                        {
                            if (!areNeighbors(node, Current) && Current.Color != node.Color && node.Color==-1)
                            {
                                node.Color = Current.Color;
                                //Current = node;
                            }
                        }
                    }
                    Color++;
                }
            }
        }
        private Boolean areNeighbors(GraphNode<T> n1, GraphNode<T> n2)
        {
            foreach (GraphNode<T> node in n1.Neighbors)
            {
                if (node == n2)
                    return true;
            }
            return false;
        }
    }
}
