﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrganisationExamenModel.Objects.Interfaces;
using OrganisationExamenModel.Objects;
using Repartition.Graph;
namespace OrganisationExamenModel.Services.Scheduler
{
    public class Scheduler
    {
        
        Graph<ScheduleItem> schedGraph = new Graph<ScheduleItem>();
        List<ScheduleItem> usedSched = new List<ScheduleItem>();
        public Scheduler()
        {
        }
        public Scheduler(ref List<Supervisor> Supervisors,ref List<Test> tests,DateTime start, DateTime end)
        {
            foreach (Test test in tests)
            {
                ScheduleItem item = new ScheduleItem();
                item.Test = test;
                schedGraph.AddNode(item);
            }
            Repartition.NodeList<ScheduleItem> items = schedGraph.Nodes;
            for (int i = 0; i < items.Count; i++)
            {
                if (usedSched.Count == 0)
                {
                    ScheduleItem item = new ScheduleItem();
                    item.Begin = start;
                    int duration = 0;
                    int.TryParse(items.ElementAt(i).Value.Test.Duration, out duration);
                    item.End = start.AddHours(duration);
                    usedSched.Add(item);
                    items.ElementAt(i).Value.Begin = item.Begin;
                    items.ElementAt(i).Value.End = item.End;
                }
            }
        }
    }
}
