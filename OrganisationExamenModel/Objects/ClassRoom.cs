﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace OrganisationExamenModel.Objects
{
    [KnownType(typeof(ClassRoom))]
    [DataContractAttribute]
    public class ClassRoom
    {
        [DataMember()]
        public String Num;
        [DataMember()]
        public String Designation;
        [DataMember()]
        public String Capacity;
        [DataMember()]
        public List<TimeBlock> FreeHours;
    }
}
