﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrganisationExamenModel.Objects
{
    public class Classe
    {
        public String ClasseName{get; set; }
        public List<Student> Students { get; set; }
        public Classe(String name)
        {
            ClasseName = name;
        }
        public Classe(String name, List<Student> students)
        {
            ClasseName = name;
            if (students != null)
                Students = students;
            else
                Students = new List<Student>();

        }
        public Boolean addStudent(Student student)
        {
            if (Students == null)
                Students = new List<Student>();
            if (!Students.Contains(student))
            {
                Students.Add(student);
                return true;
            }
            return false;
        }
    }
}
