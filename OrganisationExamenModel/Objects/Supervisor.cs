﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace OrganisationExamenModel.Objects
{
    [KnownType(typeof(Supervisor))]
    [DataContractAttribute]
    public class Supervisor
    {
        [DataMember()]
        public String FirstName{get;set;}
        [DataMember()]
        public String LastName { get; set; }
        [DataMember()]
        public String Civility { get; set; }
        [DataMember()]
        public String Cours { get; set; }
        [DataMember()]
        public String Establishment { get; set; }
        [DataMember()]
        public String ID { get; set; }
        
    }
}
