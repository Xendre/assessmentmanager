﻿using OrganisationExamenModel.Objects.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace OrganisationExamenModel.Objects
{
    [KnownType(typeof(OrganisationExamenModel.Objects.ScheduleItem))]
    [DataContractAttribute]
    public class ScheduleItem : TimeBlock
    {
        [DataMember()]
        public Test Test { get; set; }
        [DataMember()]
        public ClassRoom ClasseRoom { get; set; }
        [DataMember()]
        public List<Supervisor> Supervisors { get; set; }
        [DataMember()]
        public List<IConstraint> Constraints { get; set; }
    }
}
