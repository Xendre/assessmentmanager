﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrganisationExamenModel.Objects.Interfaces
{
    public interface IConstraint
    {
        ITimeBlock TimeRange { get; set; }
        Boolean isCompatible(IConstraint constraint);
    }
}
