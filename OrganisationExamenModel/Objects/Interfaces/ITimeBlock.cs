﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrganisationExamenModel.Objects.Interfaces
{
    public interface ITimeBlock
    {
        DateTime Begin { get; set; }
        DateTime End { get; set; }
        Boolean IsActive { get; set; }
    }
}
