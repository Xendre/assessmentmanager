﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Notifications;

namespace OrganisationExamenViewModel.Helper
{
    public class NotificationManager
    {
        public static ToastNotification DisplayTextToast(ToastTemplateType templateType, String title, String message)
        {
            String toastXmlString = "<toast>"
                               + "<visual version='1'>"
                               + "<binding template='ToastText02'>"
                               + "<text id='1'>" + title + "</text>"
                               + "<text id='2'>" + message + "</text>"
                               + "</binding>"
                               + "</visual>"
                               + "</toast>";
            Windows.Data.Xml.Dom.XmlDocument toastDOM = new Windows.Data.Xml.Dom.XmlDocument();
            toastDOM.LoadXml(toastXmlString);
            
            ToastNotification toast = new ToastNotification(toastDOM);
            ToastNotificationManager.CreateToastNotifier().Show(toast);
            return toast;
        }
    }
}
