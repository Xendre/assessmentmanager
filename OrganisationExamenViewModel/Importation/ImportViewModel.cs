﻿using OrganisationExamenModel.Services.Converter;
using OrganisationExamenViewModel.Helper;
using OrganisationExamenViewModel.ObjectsViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.Storage;
using Windows.Storage.Pickers;


namespace OrganisationExamenViewModel.Importation
{
    public class ImportViewModel : ViewModelBase
    {
       
        public ICommand ImportCommand { get; set; } 
      
        public ImportViewModel()
        {
           
            ImportCommand = new RelayCommand(new Action(() => Importer())); 
           
        }

        public async void Importer()
        {
            ConvertFiles(await Parcourir());
           
        }
        
        private async Task<IEnumerable<StorageFile>> Parcourir()
        {
            FolderPicker folderpicker = new FolderPicker();
            folderpicker.ViewMode = PickerViewMode.Thumbnail;
            folderpicker.FileTypeFilter.Add(".csv");
            folderpicker.SuggestedStartLocation = PickerLocationId.ComputerFolder;
            var folder = await folderpicker.PickSingleFolderAsync();
            IEnumerable<StorageFile> files = null;
            if(folder!=null)
                files = await folder.GetFilesAsync();

            return files;
            //var converter = CSVConverter.convertFile();
           
        }

        private async void ConvertFiles(IEnumerable<StorageFile> files)
        {
            List<ErrorView> errorList = new List<ErrorView>();
            if (files != null && files.Count() >0)
            {
                var csvfiles = (from t1 in files where t1.FileType.ToLower() == ".csv" select t1);
                foreach (var csvfile in csvfiles)
                {
                    var ofolder = ApplicationData.Current.LocalFolder;
                    var a = (await CSVConverter.convertFile(csvfile, ofolder));
                    if (a != null)
                    {
                        var e = (from t1 in a select new ErrorView() { error = t1, fileName = csvfile.Name }).ToList();
                        errorList.AddRange(e);
                    }
                }

                foreach (var e in errorList)
                    NotificationManager.DisplayTextToast(Windows.UI.Notifications.ToastTemplateType.ToastText01,e.fileName,  e.error.type.ToString()+": ligne : " + e.error.Line + " Colonne : " + e.error.Row);

               var toast = NotificationManager.DisplayTextToast(Windows.UI.Notifications.ToastTemplateType.ToastText01, "Import Terminé", errorList.Count > 0 ? "avec des erreurs" : "avec succès");
               
            }
            else
            {
                NotificationManager.DisplayTextToast(Windows.UI.Notifications.ToastTemplateType.ToastText01, "Import Annulé", "Aucun fichier");

            }

            ClassRoomViewModel.Instance.ClassRooms = await ClassRoomViewModel.LoadClassRoom();
            SupervisorViewModel.Instance.Supervisors = await SupervisorViewModel.LoadSupervisors();
            StudentViewModel.Instance.Students = await StudentViewModel.LoadStudents();
            TestViewModel.Instance.Tests = await TestViewModel.LoadTests();
        }

       
    }

    public class ErrorView
    {
        public ConvertError error { get; set; }
        public string fileName { get; set; }
    }
}
