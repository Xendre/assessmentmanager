﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace OrganisationExamenViewModel.Generation
{
    public class GenerateViewModel : ViewModelBase
    {
        public ICommand SeeGeneratePageCommand { get; set; }

        public GenerateViewModel()
        {
            SeeGeneratePageCommand = new RelayCommand(new Action(() => ChargePage())); 
        }

        public void ChargePage()
        {
            
        }
    }
}
