﻿using OrganisationExamenModel.Objects;
using OrganisationExamenModel.Services.Serializer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace OrganisationExamenViewModel.ObjectsViewModel
{
    public class TestViewModel : ViewModelBase
    {
        private static readonly TestViewModel instance = new TestViewModel();

        private TestViewModel() { Init(); }

        public static TestViewModel Instance
        {
            get
            {
                return instance;
            }
        }
        public List<TestView> Tests { get { return tests; } set { tests = value; OnPropertyChanged("Tests"); } }
        private  List<TestView> tests { get; set; }

        private async void Init()
        {
            Tests = await LoadTests();
        }

        public static async Task<List<TestView>> LoadTests()
        {
            var ofolder = ApplicationData.Current.LocalFolder;
            var output = ofolder.GetFileAsync("test.xml");
            List<object> list;
            try
            {
                list = await SerializationHelper.RestoreAsync<Test>(ofolder, await output);
            }
            catch (Exception e)
            {
                list = Enumerable.Empty<object>().ToList();
            }
            
           return (from t1 in list.Cast<Test>() select new TestView() { Name =t1.Name, Duration = t1.Duration, 
                                                                                            Type = t1.Type, Id = t1.Id
                                                                                            }).ToList();
        }

    }

    public class TestView
    {

        public String Name{get;set;}
        
        public String Duration { get; set; }
       
        public String Type { get; set; }
       
        public String Id { get; set; }
    
    }
}
