﻿using OrganisationExamenModel.Objects;
using OrganisationExamenModel.Services.Serializer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace OrganisationExamenViewModel.ObjectsViewModel
{
    public class StudentViewModel : ViewModelBase
    {
        private static readonly StudentViewModel instance = new StudentViewModel();

        private StudentViewModel() { Init(); }

        public static StudentViewModel Instance
        {
            get
            {
                return instance;
            }
        }

        public List<StudentView> Students { get { return students; } set { students = value; OnPropertyChanged("Students"); } }
        public List<StudentView> students { get; set; }


        private async  void Init()
        {
            Students = await LoadStudents();
        }
        public static async Task<List<StudentView>> LoadStudents()
        {
            var ofolder = ApplicationData.Current.LocalFolder;
            var output = ofolder.GetFileAsync("student.xml");
            List<object> list;
            try
            {
                list = await SerializationHelper.RestoreAsync<Student>(ofolder, await output);
            }
            catch (Exception e)
            {
                list = Enumerable.Empty<object>().ToList();
            }

            return  (from t1 in list.Cast<Student>()
                        select new StudentView()
                        {
                            FirstName = t1.FirstName,
                            LastName = t1.LastName,
                            Birthday = t1.Birthday,
                            ClassRoomName = t1.ClassRoomName,
                            InternNumber = t1.InternNumber,
                            Section = t1.Section
                        }).ToList();


        }

        public class StudentView
        {
            public String FirstName { get; set; }

            public String LastName { get; set; }

            public String Birthday { get; set; }

            public String ClassRoomName { get; set; }

            public String InternNumber { get; set; }

            public String Section { get; set; }

        }
    }
    
}
