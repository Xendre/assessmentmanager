﻿using OrganisationExamenModel.Objects;
using OrganisationExamenModel.Services.Serializer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace OrganisationExamenViewModel.ObjectsViewModel
{
    public sealed class SupervisorViewModel: ViewModelBase
    {
        private static readonly SupervisorViewModel instance = new SupervisorViewModel();

        private SupervisorViewModel() { Init(); }

        public static SupervisorViewModel Instance
        {
            get
            {
                return instance;
            }
        }

        public List<SupervisorView> Supervisors
        {
            get
            {
                return supervisors;
            }
            set
            {
                supervisors = value;
                OnPropertyChanged("Supervisors");

            }
        }

        private List<SupervisorView> supervisors;

        private async void Init()
        {
            Supervisors = await LoadSupervisors();
        }

        public static async Task<List<SupervisorView>> LoadSupervisors()
        {
            var ofolder = ApplicationData.Current.LocalFolder;
            var output = ofolder.GetFileAsync("supervisor.xml");
            List<object> list;
            try
            {
                list = await SerializationHelper.RestoreAsync<Supervisor>(ofolder, await output);
            }
            catch (Exception e)
            {
                list = Enumerable.Empty<object>().ToList();
            }
            return  (from t1 in list.Cast<Supervisor>() select new SupervisorView() {
                                                                                            Civility =t1.Civility, Cours = t1.Cours, 
                                                                                            Establishment = t1.Establishment, FirstName = t1.FirstName,
                                                                                            LastName = t1.LastName, ID =t1.ID}).ToList();
        }

    }

    public class SupervisorView
    {

       
        public String FirstName { get; set; }
       
        public String LastName { get; set; }
        
        public String Civility { get; set; }
        
        public String Cours { get; set; }
        
        public String Establishment { get; set; }
        
        public String ID { get; set; }
    }
}
